import java.time.LocalDate;
import java.util.*;

public class Demo {
  public static void main(String[] args) {
    Dog[] kennel = new Dog[5];

    ArrayList newKennel = new ArrayList();
    newKennel.add(new Dog("attack", LocalDate.of(2021, 5, 1)));
    newKennel.add(new Dog("bobby", LocalDate.of(2021, 5, 2)));
    Integer anInteger = Integer.valueOf(1);
    int anInt = anInteger;
    newKennel.add(1);
    newKennel.add(new Table(4, "glass"));
    for (int i = 0; i < newKennel.size(); i++) {
      System.out.println(newKennel.get(i));
    }
    if (newKennel.get(1) instanceof Dog doggy) {
      System.out.println(doggy.getName());
    }
    //  System.out.println(((Dog)newKennel.get(2)).getName());
//    for (Object o : newKennel) {
//      System.out.println(o);
//    }
    System.out.println("generics");
    System.out.println("========");
    List<Dog> genericKennel = new LinkedList<>();
    genericKennel.add(new Dog("attack", LocalDate.of(2021, 5, 13)));
    genericKennel.add(new Dog("bobby", LocalDate.of(2021, 5, 2)));
    //error because Table cannot be put in a generic collection of Dogs
    genericKennel.add(1, new Dog("Zorba", LocalDate.now()));
    for (Dog dog : genericKennel) {
      System.out.println(dog.getName());
    }
    System.out.println("iterators");
    System.out.println("========");
    for (Iterator<Dog> it = genericKennel.iterator(); it.hasNext(); ) {
      System.out.println(it.next().getName());
    }


    for (Iterator<Dog> it = genericKennel.iterator(); it.hasNext(); ) {
      if (it.next().getName().equals("attack")) {
        it.remove();
      }
    }

    System.out.println("One dog found a new home" + genericKennel);
    List<String> viruses = new ArrayList<>(List.of("gamma",
      "delta",
      "omicron",
      "xi",
      "putin",
      "biden"));
    System.out.println("sorting");
    System.out.println("========");
    System.out.println(viruses);
    Collections.sort(viruses);
    System.out.println(viruses);
    System.out.println(genericKennel);
    Collections.sort(genericKennel);
    System.out.println(genericKennel);

    System.out.println("Maps");
    System.out.println("========");
    Map<String,Dog> dogs= new HashMap<>();
    dogs.put("attack",new Dog("attack",LocalDate.of(2221,12,12)));
    Dog bobby=new Dog("bobby", LocalDate.of(2021, 5, 2));
    dogs.put(bobby.getName(),bobby);
    System.out.println(dogs);
    // get dog by name
    System.out.println(dogs.get("attack"));
    Map<LocalDate,Dog> dogsByBirthday= new HashMap<>();



  }

}
