import java.time.LocalDate;

public class Dog implements Comparable<Dog>{
  private String name;
  private LocalDate birthDay;

  @Override
  public String toString() {
    return "Dog{" +
      "name='" + name + '\'' +
      ", birthDay=" + birthDay +
      '}';
  }

  public Dog(String name, LocalDate birthDay) {
    this.name = name;
    this.birthDay = birthDay;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public LocalDate getBirthDay() {
    return birthDay;
  }

  public void setBirthDay(LocalDate birthDay) {
    this.birthDay = birthDay;
  }

  @Override
  public int compareTo(Dog other) {

  //  return birthDay.compareTo(other.birthDay);
  //  return name.compareTo(other.name);
    return  birthDay.getDayOfMonth() -other.birthDay.getDayOfMonth() ;
  }
}
