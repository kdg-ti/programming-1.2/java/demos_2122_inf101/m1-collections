import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

public class LottoEx10 {
  public static void main(String[] args) {
    Random rand = new Random();
    int uniqueNumbers = 0;
    Set<Integer> treeSet = new TreeSet<Integer>();
    while (uniqueNumbers < 6) {
      int lottoNumber = rand.nextInt(45)+1;
      boolean unique = treeSet.add(lottoNumber);
      if (unique){
        uniqueNumbers ++;
      }
    }
//    while (treeSet.size() < 6) {
//      int lottoNumber = rand.nextInt(45)+1;
//      treeSet.add(lottoNumber);
//    }
    System.out.println(treeSet);
  }
}