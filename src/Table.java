public class Table {
  int legs;
  String material;

  public Table(int legs, String material) {
    this.legs = legs;
    this.material = material;
  }

  @Override
  public String toString() {
    return "Table{" +
      "legs=" + legs +
      ", material='" + material + '\'' +
      '}';
  }
}
